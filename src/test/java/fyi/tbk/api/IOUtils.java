package fyi.tbk.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * This was borrowed from https://github.com/jasminb/jsonapi-converter/blob/develop/src/test/java/com/github/jasminb/jsonapi/IOUtils.java
 */

public class IOUtils {

    public static String getResourceAsString(String name) throws IOException {
        ClassLoader cl = IOUtils.class.getClassLoader();
        InputStream input = IOUtils.class.getClassLoader().getResourceAsStream(name);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            String line;
            StringBuilder resultBuilder = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                resultBuilder.append(line);
            }

            return resultBuilder.toString();
        }
    }

    public static InputStream getResource(String name) throws IOException {
        InputStream input = IOUtils.class.getClassLoader().getResourceAsStream(name);
        return input;
    }
}

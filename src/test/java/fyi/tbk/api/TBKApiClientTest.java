package fyi.tbk.api;

import fyi.tbk.api.errors.JsonApiErrorsException;
import fyi.tbk.api.resources.Enrollment;
import fyi.tbk.api.resources.User;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.List;

public class TBKApiClientTest {

    private MockWebServer server;
    private TBKApiClient client;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() throws IOException {
        server = new MockWebServer();
        server.start();
        client = new TBKApiClient(server.url("/").toString(), "TOKEN");
    }

    @Test
    public void testListUsersSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(200, IOUtils.getResourceAsString("users.json"));
        List<User> users = client.listUsers("a-unique_id");
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void testGetUserSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(200, IOUtils.getResourceAsString("user.json"));
        User users = client.getUser(1234);
        Assert.assertEquals("1234", users.getId());
        Assert.assertEquals("monica.hall@pied-piper.local", users.getEmail());
        Assert.assertEquals("my-academy", users.getAcademy().getSubdomain());
    }

    @Test
    public void testGetUserNotFound() throws IOException, JsonApiErrorsException {
        mockResponse(404, IOUtils.getResourceAsString("error-not_found.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.getUser(1234);
    }

    @Test
    public void testDeleteUserSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(204, null);
        Assert.assertTrue(client.deleteUser(1234));
    }

    @Test
    public void testDeleteUserForbidden() throws IOException, JsonApiErrorsException {
        mockResponse(403, IOUtils.getResourceAsString("error-forbidden.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.deleteUser(1234);
    }

    @Test
    public void testGetUserEnrollmentsSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(200, IOUtils.getResourceAsString("enrollments.json"));
        List<Enrollment> enrollments = client.getUserEnrollments(1234);
        Assert.assertEquals(1, enrollments.size());
    }

    @Test
    public void testGetUserEnrollmentsNotFound() throws IOException, JsonApiErrorsException {
        mockResponse(404, IOUtils.getResourceAsString("error-not_found.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.getUserEnrollments(1234);
    }

    @Test
    public void testGetEnrollmentSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(200, IOUtils.getResourceAsString("enrollment.json"));
        Enrollment enrollment = client.getEnrollment(1234);
        Assert.assertEquals("1234", enrollment.getId());
        Assert.assertEquals("how-to-sleep", enrollment.getCourseIdentifier());
        Assert.assertEquals(1, enrollment.getUserActivities().size());
    }

    @Test
    public void testGetEnrollmentNotFound() throws IOException, JsonApiErrorsException {
        mockResponse(404, IOUtils.getResourceAsString("error-not_found.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.getEnrollment(1235);
    }

    @Test
    public void testDeleteEnrollmentSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(204, null);
        Assert.assertTrue(client.deleteEnrollment(1234));
    }

    @Test
    public void testDeleteEnrollmentForbidden() throws IOException, JsonApiErrorsException {
        mockResponse(403, IOUtils.getResourceAsString("error-forbidden.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.deleteEnrollment(1234);
    }

    @Test
    public void testDeleteEnrollmentActivitiesSuccess() throws IOException, JsonApiErrorsException {
        mockResponse(204, null);
        Assert.assertTrue(client.deleteEnrollmentActivities(1234, 123, 1234));
    }

    @Test
    public void testDeleteEnrollmentActivitiesForbidden() throws IOException, JsonApiErrorsException {
        mockResponse(403, IOUtils.getResourceAsString("error-forbidden.json"));
        thrown.expect(JsonApiErrorsException.class);
        thrown.expectMessage("An Error occurred while talk to the API.");
        client.deleteEnrollmentActivities(1234, 1234, 12344);
    }

    private void mockResponse(int status, String body) {
        MockResponse resp = new MockResponse().setResponseCode(status);
        if (body != null) {
            resp.setBody(body);
        }
        server.enqueue(resp);
    }
}

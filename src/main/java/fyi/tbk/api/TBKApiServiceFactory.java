package fyi.tbk.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.github.jasminb.jsonapi.retrofit.JSONAPIConverterFactory;
import fyi.tbk.api.resources.Academy;
import fyi.tbk.api.resources.Enrollment;
import fyi.tbk.api.resources.User;
import fyi.tbk.api.resources.UserActivity;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;

public class TBKApiServiceFactory {

    private static final String UserAgent = "TBK-Java-Client";
    private static final String ContentType = "application/vnd.api+json";

    private Retrofit.Builder builder;
    private OkHttpClient.Builder httpClient;
    private String baseUrl;
    private String token;

    public static TBKApiService createService(String baseUrl, String token) {
        return new TBKApiServiceFactory(baseUrl, token).build();
    }

    TBKApiServiceFactory(String baseUrl, String token) {
        this.baseUrl = baseUrl;
        this.token = "Bearer " + token;
        this.builder = new Retrofit.Builder();
        this.httpClient = new OkHttpClient.Builder();
    }

    public TBKApiService build() {
        configureHttpHeaders();
        return buildRetrofit().create(TBKApiService.class);
    }

    private Retrofit buildRetrofit() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        return builder.client(httpClient.build())
                .baseUrl(baseUrl)
                .addConverterFactory(new JSONAPIConverterFactory(mapper, User.class, Academy.class, Enrollment.class, UserActivity.class))
                .build();
    }

    private void configureHttpHeaders() {
        httpClient.interceptors().clear();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder1 = original.newBuilder()
                    .header("User-Agent", UserAgent)
                    .header("Accept", ContentType)
                    .header("Authorization", token);
            Request request = builder1.build();
            return chain.proceed(request);
        });
    }

}

package fyi.tbk.api.errors;

import com.github.jasminb.jsonapi.models.errors.Error;
import com.github.jasminb.jsonapi.models.errors.Errors;

import java.util.List;

public class JsonApiErrorsException extends Exception {
    private Errors errors;

    public JsonApiErrorsException(String s, Errors errors) {
        super(s);
        this.errors = errors;
    }

    public List<Error> getErrors() {
        return errors.getErrors();
    }
}

package fyi.tbk.api;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import fyi.tbk.api.resources.Enrollment;
import fyi.tbk.api.resources.User;
import fyi.tbk.api.resources.UserActivity;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;


public interface TBKApiService {

    String USERS_PATH = "api/public/v1/users";
    String USER_PATH = "api/public/v1/users/{id}";
    String USER_ENROLLMENTS_PATH = "api/public/v1/users/{user_id}/enrollments";
    String ENROLLMENT_PATH = "api/public/v1/enrollments/{id}";
    String ENROLLMENT_ACTIVITIES_PATH = "api/public/v1/enrollments/{id}/relationships/activities";

    @GET(USERS_PATH)
    Call<JSONAPIDocument<List<User>>> listUsers(@Query("filter[uuid]") String uuids);

    @GET(USER_PATH)
    Call<JSONAPIDocument<User>> getUser(@Path("id") int id);

    @GET(USER_PATH)
    Call<JSONAPIDocument<User>> getUser(@Path("id") int id, @Query("include") String includedRelations);

    @DELETE(USER_PATH)
    Call<Void> deleteUser(@Path("id") int id);

    @GET(USER_ENROLLMENTS_PATH)
    Call<JSONAPIDocument<List<Enrollment>>> getUserEnrollments(@Path("user_id") int user_id);

    @GET(ENROLLMENT_PATH)
    Call<JSONAPIDocument<Enrollment>> getEnrollment(@Path("id") int id);

    @DELETE(ENROLLMENT_PATH)
    Call<Void> deleteEnrollment(@Path("id") int id);

    @HTTP(method = "DELETE", path = ENROLLMENT_ACTIVITIES_PATH, hasBody = true)
    Call<Void> deleteEnrollmentActivities(@Path("id") int id, @Body List<UserActivity> userActivities);

}

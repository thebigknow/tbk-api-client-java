package fyi.tbk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jasminb.jsonapi.ErrorUtils;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.models.errors.Errors;
import fyi.tbk.api.errors.JsonApiErrorsException;
import fyi.tbk.api.resources.*;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TBKApiClient {

    private TBKApiService svc;

    TBKApiClient(String baseUrl, String token) {
        svc = TBKApiServiceFactory.createService(baseUrl, token);
    }

    public List<User> listUsers(String... uuids) throws IOException, JsonApiErrorsException {
        Response<JSONAPIDocument<List<User>>> svcResponse = svc.listUsers(String.join(",", uuids)).execute();
        handleError(svcResponse);
        return svcResponse.body().get();
    }

    public User getUser(int id) throws IOException, JsonApiErrorsException {
        Response<JSONAPIDocument<User>> svcResponse = svc.getUser(id).execute();
        handleError(svcResponse);
        return svcResponse.body().get();
    }

    public User getUser(int id, String includes) throws IOException, JsonApiErrorsException {
        Response<JSONAPIDocument<User>> svcResponse = svc.getUser(id, includes).execute();
        handleError(svcResponse);
        return svcResponse.body().get();
    }

    public boolean deleteUser(int userId) throws IOException, JsonApiErrorsException {
        Response<Void> svcResponse = svc.deleteUser(userId).execute();
        handleError(svcResponse);
        return true;
    }

    public List<Enrollment> getUserEnrollments(int userId) throws IOException, JsonApiErrorsException {
        Response<JSONAPIDocument<List<Enrollment>>> svcResponse = svc.getUserEnrollments(userId).execute();
        handleError(svcResponse);
        return svcResponse.body().get();
    }

    public Enrollment getEnrollment(int enrollmentId) throws IOException, JsonApiErrorsException {
        Response<JSONAPIDocument<Enrollment>> svcResponse = svc.getEnrollment(enrollmentId).execute();
        handleError(svcResponse);
        return svcResponse.body().get();
    }

    public boolean deleteEnrollment(int enrollmentId) throws IOException, JsonApiErrorsException {
        Response<Void> svcResponse = svc.deleteEnrollment(enrollmentId).execute();
        handleError(svcResponse);
        return true;
    }

    public boolean deleteEnrollmentActivities(int enrollmentId, int... activityIds) throws IOException, JsonApiErrorsException {
        List<UserActivity> activities = new ArrayList<>();
        for (int i : activityIds) {
            UserActivity activity = new UserActivity();
            activity.setId(Integer.toString(i));
            activities.add(activity);
        }
        Response<Void> svcResponse = svc.deleteEnrollmentActivities(enrollmentId, activities).execute();
        handleError(svcResponse);
        return true;
    }

    private void handleError(Response response) throws IOException, JsonApiErrorsException {
        if (response.isSuccessful()) return;

        if (response.raw().code() < 500 && response.errorBody() != null) {
            Errors errors = ErrorUtils.parseErrorResponse(new ObjectMapper(), response.errorBody(), Errors.class);
            throw new JsonApiErrorsException("An Error occurred while talk to the API.", errors);
        } else {
            throw new IOException("Something went wrong. Status code: " + response.raw().code());
        }
    }

}

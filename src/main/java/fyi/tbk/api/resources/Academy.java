package fyi.tbk.api.resources;

import com.github.jasminb.jsonapi.annotations.Type;

@Type("academies")
public class Academy extends BaseModel {

    private String name;
    private String subdomain;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

}

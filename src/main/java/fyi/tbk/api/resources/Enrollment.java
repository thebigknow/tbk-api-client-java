package fyi.tbk.api.resources;

import com.github.jasminb.jsonapi.annotations.Type;
import com.github.jasminb.jsonapi.annotations.Relationship;

import java.util.List;

@Type("enrollments")
public class Enrollment extends BaseModel {

    private String courseIdentifier;
    private String courseName;
    private String enrollmentDate;

    @Relationship("user")
    private User user;

    @Relationship("user_activities")
    private List<UserActivity> userActivities;

    public String getCourseIdentifier() {
        return courseIdentifier;
    }

    public void setCourseIdentifier(String courseIdentifier) {
        this.courseIdentifier = courseIdentifier;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(String enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public User getUser() {
        return user;
    }

    public List<UserActivity> getUserActivities() {
        return userActivities;
    }

}

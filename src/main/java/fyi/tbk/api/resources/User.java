package fyi.tbk.api.resources;

import com.github.jasminb.jsonapi.annotations.Type;
import com.github.jasminb.jsonapi.annotations.Relationship;

import java.util.List;

@Type("users")
public class User extends BaseModel {

    private String uuid;
    private String email;
    private String firstName;
    private String lastName;

    @Relationship("academy")
    private Academy academy;

    @Relationship("enrollments")
    private List<Enrollment> enrollments;


    public Academy getAcademy() {
        return academy;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Enrollment> getEnrollments() {
        return enrollments;
    }

}

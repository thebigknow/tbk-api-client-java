package fyi.tbk.api.resources;

import com.github.jasminb.jsonapi.annotations.Id;

public class BaseModel {
    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

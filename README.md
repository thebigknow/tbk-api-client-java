# TBK Java API Client

This client was built to talk to the TBK Public API v1. This file demonstrates how to initialize and use supported calls to the API.

You can find the Swagger API documentation for the API here: https://app.swaggerhub.com/apis/dwanek/tbk-public-api-v1/

Access to resources is scoped to certain academies based on the API token. So when you make calls to the API it is possible to talk to any Academies you are scoped to.

## Initialization

```java
private String apiUrl = <URL provided by TBK>;
private String jwtToken = <Token provided by TBK>;
TBKApiClient client = new TBKApiClient(apiUrl, token);
```

## Users

### Listing by UUID

If you know a user's UUID you can do a listing for them to see which academies they are in based on your current scope.

```java
List<User> users = client.listUsers("some-uuid");
for(User u : users) {
    System.out.println("User.id: " + u.getId() + ", Academy: " + u.getAcademy().getSubdomain());
}
```

### Getting a specific user by Id

Getting a user by Id gives you basically the same information as `listUsers` however you can add additional options to the call so you can get all of the enrollment and activity data in one call.

```java
User user = client.getUser(1234, "enrollments.user_activites");
// You can then access them like so:
List<Enrollment> enrollments = user.getEnrollments();
List<UserActivity> activities = enrollments.get(0).getUserActivities();
```

### Deleting a User

If your API Token has the role to delete you can remove the User by Id. This will completely remove the user from the system including all enrollment data and activities.

```java
client.deleteUser(1234);
```


## Enrollments

Access to enrollments is very similar to User with the exception of enumerating the enrollments. That must be done through the User. Enrollments by default include all of the information about UserActivities. If the user has not yet done
anything with a specific Activity the `id` will be a transient `id` in the format `no-id-<randomHex>`.

### Listing Enrollments

```java
List<Enrollment> enrollments = client.getEnrollments(userId);
```

### Getting an Enrollment by Id

```java
Enrollment enrollment = client.getEnrollment(id);
```

### Deleting an Enrollment by Id

This operation will completely remove a user enrollment and activity data for the given user and course. This includes discussion activities and comments on those discussion posts. It will wipe the slate clean for this course + user.

```java
client.deleteEnrollment(id);
```

### Deleting User Activities for an Enrollment

This operation will remove activities and completion data for the given Enrollment. If no request body is given all activities except for Discussion activities will be removed.
 
**Note**: The user will stay enrolled in the course with this call.

#### Removal of all UserActivities except Discussion

```java
client.deleteEnrollmentActivities(enrollmentId);
```

#### Removal of specific UserActivities

Unlike the call without specifying activityIds, this call will remove Discussion activities if specified. If a discussion activity is removed all responses to the thread will also be removed.

```java
client.deleteEnrollmentActivities(enrollmentId, id1, id2 ... idN);
```
